This repository contains code to convert color measurements between different color spaces and plot them on a CIE 1931 diagram.
Currently, conversions are based on D65 illuminant.

This program was developed on Python3 and PyQt4.

Colorspace (Corresponding column names):
* Lab (L, a, b)
* XYZ (X, Y, Z)
* sRGB (sR, sG, sB)
* xy (x, y)

# Version - v1.1.0
1. Import can handle XYZ, sRGB, and xy imports
2. Export colorspace conversion data for Lab, XYZ, sRGB, and xy for each dataset, or merge all into a single dataset

# Version - v1.0.0
## Released - Nov 9, 2018

1. Import up to 4 LAB datasets
2. Converts to XY colorspace and plots all 4 datasets, with unique identifiers on single CIE 1931 Chromaticity Diagram