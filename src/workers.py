from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtCore import QObject, QThread, pyqtSignal, QTimer, pyqtSlot
import matplotlib
matplotlib.use('Qt4Agg')
from matplotlib import pyplot as plt

import pathlib
import sys
import time
import numpy as np
import pandas as pd
import pyqtgraph as pg
import colour
import constants
from colour import plotting
from colour.colorimetry import ILLUMINANTS
import logging



class SystemWorker(QThread):

    signal_status = pyqtSignal(str)

    signal_done_plot_chroma = pyqtSignal(int, list, list)
    signal_done_plot_all_chroma = pyqtSignal(list, list)

    signal_error_import_colorspace = pyqtSignal(str)

    def __init__(self, instruction_queue):
        QThread.__init__(self)

        # logging.basicConfig(level=logging.INFO)
        # self.logger = logging.getLogger(__name__)
        self.instruction_queue = instruction_queue
        self.num_datasets = 0

        self.shutdown = False
        self.my_datasets = {}

    def __del__(self):
        self.wait()

    def run(self):
        print('worker: run started')
        while not self.shutdown:
            for i in range(self.instruction_queue.qsize()):
                instr = self.instruction_queue.get()
                exec('self.' + instr)
            time.sleep(0.1)
        print('worker: run finished')

    def shutdown_worker(self):
        self.shutdown = True

    def replace_dataset(self, index, filepath, colorspace):
        # self.logger.info('worker: replace dataset')
        self.my_datasets[index] = pd.DataFrame.from_csv(filepath, index_col=False)
        if colorspace == 'sRGB':
            if not 'sR' in self.my_datasets[index] or not 'sG' in self.my_datasets[index] or not 'sB' in self.my_datasets[index]:
                # self.logger.warning('worker: Error: one or all RGB columns are missing!')
                self.signal_error_import_colorspace.emit('RGB')

            else:
                L,a,b = self.convert_sRGB_to_Lab(self.my_datasets[index]['sR'],
                                                 self.my_datasets[index]['sG'],
                                                 self.my_datasets[index]['sB'])

                self.my_datasets[index]['L'] = L
                self.my_datasets[index]['a'] = a
                self.my_datasets[index]['b'] = b

        elif colorspace == 'XYZ':
            if not 'X' in self.my_datasets[index] or not 'Y' in self.my_datasets[index] or not 'Z' in self.my_datasets[index]:
                # self.logger.warning('worker: Error: one or all XYZ columns are missing!')
                self.signal_error_import_colorspace.emit('XYZ')
            else:
                L,a,b = self.convert_XYZ_to_Lab(self.my_datasets[index]['X'],
                                                self.my_datasets[index]['Y'],
                                                self.my_datasets[index]['Z'])

                self.my_datasets[index]['L'] = L
                self.my_datasets[index]['a'] = a
                self.my_datasets[index]['b'] = b
        elif colorspace == 'xy':
            if not 'x' in self.my_datasets[index] or not 'y' in self.my_datasets[index]:
                # self.logger.warning('worker: Error: one or all xy columns are missing!')
                self.signal_error_import_colorspace.emit('xy')
            else:
                L,a,b = self.convert_xy_to_Lab(self.my_datasets[index]['x'],
                                               self.my_datasets[index]['y'])

                self.my_datasets[index]['L'] = L
                self.my_datasets[index]['a'] = a
                self.my_datasets[index]['b'] = b
        elif colorspace == 'Lab':
            if not 'L' in self.my_datasets[index] or not 'a' in self.my_datasets[index] or not 'b' in self.my_datasets[index]:
                # self.logger.warning('worker: Error: one or all Lab columns are missing!')
                self.signal_error_import_colorspace.emit('Lab')
        else:
            self.signal_status.emit('Dataset replacement was a success!')

    def add_dataset(self, filepath, colorspace):
        self.num_datasets += 1
        # self.logger.info('worker: import dataset')
        self.my_datasets[self.num_datasets] = pd.read_csv(filepath, index_col=False)
        if colorspace == 'sRGB':
            if not 'sR' in self.my_datasets[self.num_datasets] or not 'sG' in self.my_datasets[self.num_datasets] or not 'sB' in self.my_datasets[self.num_datasets]:
                # self.logger.warning('worker: Error: one or all RGB columns are missing!')
                self.signal_error_import_colorspace.emit('RGB')

            else:
                L,a,b = self.convert_sRGB_to_Lab(self.my_datasets[self.num_datasets]['sR'],
                                                 self.my_datasets[self.num_datasets]['sG'],
                                                 self.my_datasets[self.num_datasets]['sB'])

                self.my_datasets[self.num_datasets]['L'] = L
                self.my_datasets[self.num_datasets]['a'] = a
                self.my_datasets[self.num_datasets]['b'] = b

        elif colorspace == 'XYZ':
            if not 'X' in self.my_datasets[self.num_datasets] or not 'Y' in self.my_datasets[self.num_datasets] or not 'Z' in self.my_datasets[self.num_datasets]:
                # self.logger.warning('worker: Error: one or all XYZ columns are missing!')
                self.signal_error_import_colorspace.emit('XYZ')
            else:
                L,a,b = self.convert_XYZ_to_Lab(self.my_datasets[self.num_datasets]['X'],
                                                self.my_datasets[self.num_datasets]['Y'],
                                                self.my_datasets[self.num_datasets]['Z'])

                self.my_datasets[self.num_datasets]['L'] = L
                self.my_datasets[self.num_datasets]['a'] = a
                self.my_datasets[self.num_datasets]['b'] = b
        elif colorspace == 'xy':
            if not 'x' in self.my_datasets[self.num_datasets] or not 'y' in self.my_datasets[self.num_datasets]:
                # self.logger.warning('worker: Error: one or all xy columns are missing!')
                self.signal_error_import_colorspace.emit('xy')
            else:
                L,a,b = self.convert_xy_to_Lab(self.my_datasets[self.num_datasets]['x'],
                                               self.my_datasets[self.num_datasets]['y'])

                self.my_datasets[self.num_datasets]['L'] = L
                self.my_datasets[self.num_datasets]['a'] = a
                self.my_datasets[self.num_datasets]['b'] = b
        elif colorspace == 'Lab':
            if not 'L' in self.my_datasets[self.num_datasets] or not 'a' in self.my_datasets[self.num_datasets] or not 'b' in self.my_datasets[self.num_datasets]:
                # self.logger.debug('worker: Error: one or all Lab columns are missing!')
                self.signal_error_import_colorspace.emit('Lab')
        else:
            self.signal_status.emit('Dataset Import was a success!')

    def remove_dataset(self, index):
        self.my_datasets.pop(index,0)
        # self.logger.debug(self.my_datasets)

    def move_dataset(self, origin, target):
        self.my_datasets[target] = self.my_datasets[origin]
        self.remove_dataset(origin)

    def reduce_dataset(self):
        self.num_datasets -= 1

    def plot_all(self):
        # self.logger.info('worker: plot all')
        all_x = []
        all_y = []

        for index in range(self.num_datasets):
            L = self.my_datasets[index+1]['L']
            a = self.my_datasets[index+1]['a']
            b = self.my_datasets[index+1]['b']
            X,Y,Z = self.convert_Lab_to_XYZ(L,a,b)
            x, y = self.convert_XYZ_to_xy(X,Y,Z)
            all_x.append(x)
            all_y.append(y)
        self.signal_done_plot_all_chroma.emit(all_x, all_y)

    def preview(self, index):
        valid = self.is_valid_index(index)

        if valid:
            obs_x = []
            obs_y = []
            # self.logger.debug('worker: plot {}'.format(index))
            L = self.my_datasets[index]['L']
            a = self.my_datasets[index]['a']
            b = self.my_datasets[index]['b']
            X,Y,Z = self.convert_Lab_to_XYZ(L,a,b)
            x,y = self.convert_XYZ_to_xy(X,Y,Z)
            self.signal_done_plot_chroma.emit(index, x, y)
        else:
            self.signal_status.emit('Error: Dataset has not been imported!')

    def separate_make_colorspaces(self, save_location, filenames):
        for i in range(0, self.num_datasets):
            L = self.my_datasets[i+1]['L']
            a = self.my_datasets[i+1]['a']
            b = self.my_datasets[i+1]['b']
            X, Y, Z = self.convert_Lab_to_XYZ(L,a,b)
            sR, sG, sB = self.convert_XYZ_to_sRGB(X,Y,Z)
            x, y = self.convert_XYZ_to_xy(X,Y,Z)
            

            data = np.column_stack((L, a, b,
                                    sR, sG, sB,
                                    X, Y, Z,
                                    x, y))
            headers = 'L,a,b,sR,sG,sB,X,Y,Z,x,y'
            save_filename = save_location + '\\' + filenames[i]
            save_filename = save_filename[:-4] + '_converted.csv'
            np.savetxt(save_filename, data, header = headers, delimiter=',', fmt = '%0.6f', comments='')

    def merge_make_colorspaces(self, save_location):
        for i in range(0, self.num_datasets):
            L = self.my_datasets[i+1]['L']
            a = self.my_datasets[i+1]['a']
            b = self.my_datasets[i+1]['b']
            X, Y, Z = self.convert_Lab_to_XYZ(L,a,b)
            sR, sG, sB = self.convert_XYZ_to_sRGB(X,Y,Z)
            x, y = self.convert_XYZ_to_xy(X,Y,Z)
            
            self.my_datasets[i+1]['L'] = L
            self.my_datasets[i+1]['a'] = a
            self.my_datasets[i+1]['b'] = b
            self.my_datasets[i+1]['X'] = X
            self.my_datasets[i+1]['Y'] = Y
            self.my_datasets[i+1]['Z'] = Z
            self.my_datasets[i+1]['sR'] = sR
            self.my_datasets[i+1]['sG'] = sG
            self.my_datasets[i+1]['sB'] = sB
            self.my_datasets[i+1]['x'] = x
            self.my_datasets[i+1]['y'] = y

            if i == 0:
                merged_dataset = self.my_datasets[i+1]
            else:
                merged_dataset = merged_dataset.append(self.my_datasets[i+1])

        # merged_df = pd.concat(merged_dataset)

        data = np.column_stack((merged_dataset['L'], merged_dataset['a'], merged_dataset['b'],
                                merged_dataset['sR'], merged_dataset['sG'], merged_dataset['sB'],
                                merged_dataset['X'], merged_dataset['Y'], merged_dataset['Z'],
                                merged_dataset['x'], merged_dataset['y']))
        headers = 'L,a,b,sR,sG,sB,X,Y,Z,x,y'
        np.savetxt(save_location + '\\merged_converted.csv', data, header = headers, delimiter=',', fmt = '%0.6f', comments='')

    def is_valid_index(self, index):
        if index > self.num_datasets:
            return False
        else:
            return True

    def convert_sRGB_to_Lab(self, 
                            sR, sG, sB, 
                            view_angle = constants.REFERENCE_ANGLE[2], 
                            white_reference = constants.WHITE_REFERENCE_D50):
        data = np.column_stack((sR/255., sG/255., sB/255.))
        conv_L = []
        conv_a = []
        conv_b = []
        for i in data:
            X,Y,Z = colour.sRGB_to_XYZ(i, 
                illuminant = ILLUMINANTS[view_angle][white_reference])
            L,a,b = colour.XYZ_to_Lab([X,Y,Z],
                illuminant = ILLUMINANTS[view_angle][white_reference])
            conv_L.append(L)
            conv_a.append(a)
            conv_b.append(b)
        return conv_L, conv_a, conv_b

    def convert_XYZ_to_sRGB(self, 
                            X, Y, Z,
                            view_angle = constants.REFERENCE_ANGLE[2], 
                            white_reference = constants.WHITE_REFERENCE_D50):
        data = np.column_stack((X, Y, Z))
        conv_X = []
        conv_Y = []
        conv_Z = []
        for i in data:
            sR, sG, sB = colour.XYZ_to_sRGB(i,
                illuminant = ILLUMINANTS[view_angle][white_reference])
            conv_X.append(sR*255)
            conv_Y.append(sG*255)
            conv_Z.append(sB*255)
        return conv_X, conv_Y, conv_Z

    def convert_XYZ_to_Lab(self, 
                           X, Y, Z,
                           view_angle = constants.REFERENCE_ANGLE[2], 
                           white_reference = constants.WHITE_REFERENCE_D50):
        data = np.column_stack((X, Y, Z))
        conv_L = []
        conv_a = []
        conv_b = []
        for i in data:
            L,a,b = colour.XYZ_to_LAB(i, 
                illuminant = ILLUMINANTS[view_angle][white_reference])
            conv_L.append(L)
            conv_a.append(a)
            conv_b.append(b)
        return conv_L, conv_a, conv_b

    def convert_XYZ_to_xy(self, 
                          X, Y, Z,
                          view_angle = constants.REFERENCE_ANGLE[2], 
                          white_reference = constants.WHITE_REFERENCE_D50):
        data = np.column_stack((X, Y, Z))
        conv_x = []
        conv_y = []
        for i in data:
            x,y = colour.XYZ_to_xy(i, 
                illuminant = ILLUMINANTS[view_angle][white_reference])
            conv_x.append(x)
            conv_y.append(y)
        return conv_x, conv_y

    def convert_Lab_to_XYZ(self, 
                           L,a,b,
                           view_angle = constants.REFERENCE_ANGLE[2], 
                           white_reference = constants.WHITE_REFERENCE_D50):
        data = np.column_stack((L,a,b))
        conv_X = []
        conv_Y = []
        conv_Z = []
        for i in data:
            X,Y,Z = colour.Lab_to_XYZ(i,
                illuminant = ILLUMINANTS[view_angle][white_reference])
            conv_X.append(X)
            conv_Y.append(Y)
            conv_Z.append(Z)
        return conv_X, conv_Y, conv_Z

    def convert_xy_to_Lab(self, x, y):
        data = np.column_stack((x,y))
        conv_L = []
        conv_a = []
        conv_b = []
        for i in data:
            X, Y, Z = colour.xy_to_XYZ(i,
                illuminant = ILLUMINANTS[view_angle][white_reference])
            L,a,b = colour.XYZ_to_LAB(X, Y, Z,
                illuminant = ILLUMINANTS[view_angle][white_reference])
            conv_L.append(L)
            conv_a.append(a)
            conv_b.append(b)
        return conv_L, conv_a, conv_b