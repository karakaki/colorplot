from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtCore import QObject, QThread, pyqtSignal, QTimer
from PyQt4.QtGui import QMessageBox, QFileDialog
from matplotlib import pyplot as plt
from queue import Queue
import pathlib
import sys
import time
import numpy as np
import pandas as pd
import pyqtgraph as pg
import colour
from colour import plotting
from workers import SystemWorker
from constants import *
import pylab

class PlotLABApp(QtGui.QMainWindow):
    logger = pyqtSignal('QString')


    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.ui = uic.loadUi('main.ui',self)
        self.ui.show()


        self.instruction_queue = Queue()

        self.ui.button_exit.clicked.connect(self.exit)
        # self.ui.button_import.clicked.connect(self.import_dataset)
        self.ui.button_plotAll.clicked.connect(self.plot_all)
        self.ui.button_color1.clicked.connect(self.select_color1)
        self.ui.button_color2.clicked.connect(self.select_color2)
        self.ui.button_color3.clicked.connect(self.select_color3)
        self.ui.button_color4.clicked.connect(self.select_color4)
        self.ui.button_preview1.clicked.connect(self.preview1)
        self.ui.button_preview2.clicked.connect(self.preview2)
        self.ui.button_preview3.clicked.connect(self.preview3)
        self.ui.button_preview4.clicked.connect(self.preview4)
        self.ui.button_remove1.clicked.connect(self.remove1)
        self.ui.button_remove2.clicked.connect(self.remove2)
        self.ui.button_remove3.clicked.connect(self.remove3)
        self.ui.button_remove4.clicked.connect(self.remove4)
        self.ui.button_import1.clicked.connect(self.import1)
        self.ui.button_import2.clicked.connect(self.import2)
        self.ui.button_import3.clicked.connect(self.import3)
        self.ui.button_import4.clicked.connect(self.import4)
        self.ui.button_convertColorspace.clicked.connect(self.separate_make_colorspaces)
        self.ui.button_mergeConvertColorspace.clicked.connect(self.merge_make_colorspaces)

        self.threads = {}
        self.threads['plot'] = QThread()
        self.workerThread = SystemWorker(self.instruction_queue)
        self.workerThread.moveToThread(self.threads['plot'])

        self.workerThread.signal_status.connect(self.worker_status, QtCore.Qt.QueuedConnection)
        self.workerThread.signal_done_plot_chroma.connect(self.done_plot_chroma, QtCore.Qt.QueuedConnection)
        self.workerThread.signal_done_plot_all_chroma.connect(self.done_plot_all_chroma, QtCore.Qt.QueuedConnection)
        self.workerThread.signal_error_import_colorspace.connect(self.error_import_colorspace, QtCore.Qt.QueuedConnection)

        for thread in self.threads.values():
            thread.start()

        QTimer.singleShot(0, self.workerThread.run)

        self.num_datasets = 0
        self.color1 = 'black'
        self.color2 = 'black'
        self.color3 = 'black'
        self.color4 = 'black'
        self.color = ['black',
                      'black',
                      'black',
                      'black']
        self.valid_filenames = []

    def done_plot_chroma(self, index, obs_x, obs_y):
        """Receive dataset from worker, plot single dataset CIE with user settings
        """
        # self.worker_status('Plotting Dataset {}...'.format(index))
        colour.plotting.plot_chromaticity_diagram_CIE1931(standalone=False)
        if index == 1:
            pylab.scatter(obs_x, obs_y,
                          marker=self.ui.comboBox_style1.currentText(),
                          color=self.color[index-1])
        elif index == 2:
            pylab.scatter(obs_x, obs_y,
                          marker=self.ui.comboBox_style2.currentText(),
                          color=self.color[index-1])
        elif index == 3:
            pylab.scatter(obs_x, obs_y,
                          marker=self.ui.comboBox_style3.currentText(),
                          color=self.color[index-1])
        elif index == 4:
            pylab.scatter(obs_x, obs_y,
                          marker=self.ui.comboBox_style4.currentText(),
                          color=self.color[index-1])
        plt.gcf().set_size_inches((8, 8))
        plt.xlim(-0.1, 0.9)
        plt.ylim(-0.1, 0.9)
        plt.show()

    def done_plot_all_chroma(self, all_x, all_y):
        """Receive datasets from worker thread, plot all datasets on CIE with user settings
        """
        # self.worker_status('Plotting All Datasets...')
        colour.plotting.plot_chromaticity_diagram_CIE1931(standalone=False)
        for i in range(len(all_x)):
            if i == 0:
                pylab.scatter(all_x[i], all_y[i],
                              marker=self.ui.comboBox_style1.currentText(),
                              color=self.color[i])
            elif i == 1:
                pylab.scatter(all_x[i], all_y[i],
                              marker=self.ui.comboBox_style2.currentText(),
                              color=self.color[i])
            elif i == 2:
                pylab.scatter(all_x[i], all_y[i],
                              marker=self.ui.comboBox_style3.currentText(),
                              color=self.color[i])
            elif i == 3:
                pylab.scatter(all_x[i], all_y[i],
                              marker=self.ui.comboBox_style4.currentText(),
                              color=self.color[i])
        plt.gcf().set_size_inches((8,8))
        plt.xlim(-0.1, 0.9)
        plt.ylim(-0.1, 0.9)
        plt.show()

    def preview1(self):
        self.instruction_queue.put('preview({})'.format(1))
    def preview2(self):
        self.instruction_queue.put('preview({})'.format(2))
    def preview3(self):
        self.instruction_queue.put('preview({})'.format(3))
    def preview4(self):
        self.instruction_queue.put('preview({})'.format(4))

    def import1(self):
        self.import_file(1)
    def import2(self):
        self.import_file(2)
    def import3(self):
        self.import_file(3)
    def import4(self):
        self.import_file(4)

    def import_file(self,index):
        filepath = ''
        filepath = QtGui.QFileDialog.getOpenFileName(self, 'Select a file','*.csv')
        if filepath == '':
            self.ui.statusbar.showMessage('Please Select a file!')
        else:
            # replacing existing dataset
            if index <= self.num_datasets:
                filename = filepath.split("/")
                self.set_filepathText(index, filename[-1])
                self.valid_filenames[index-1] = filename[-1]
                self.ui.statusbar.showMessage('Loading Dataset {}...'.format(index))
                # self.instruction_queue.put("add_dataset(r'{}',r'{}')".format(filepath, self.ui.comboBox_importColorspace.currentText()))
                self.instruction_queue.put("replace_dataset({}, r'{}',r'{}')".format(index, filepath, self.ui.comboBox_importColorspace.currentText()))
            # adding new dataset
            else:
                self.num_datasets += 1
                filename = filepath.split("/")
                self.set_filepathText(self.num_datasets, filename[-1])
                self.valid_filenames.append(filename[-1])
                self.ui.statusbar.showMessage('Loading Dataset {}...'.format(self.num_datasets))
                self.instruction_queue.put("add_dataset(r'{}',r'{}')".format(filepath, self.ui.comboBox_importColorspace.currentText()))
            

    def remove_dataset(self, index):
        """Remove a dataset from the list of 4, pop and shift all remaining datasets if necessary"""

        self.valid_filenames.pop(index-1)
        if index == 1:
            if self.num_datasets == 1:
                self.ui.label_filepath1.setText('')
                self.ui.label_color1.setStyleSheet("background-color: " + str(None))
                self.color[index-1] = DEFAULT_DATASET_COLOR
                self.instruction_queue.put('remove_dataset({})'.format(1))
                self.num_datasets -= 1
                self.instruction_queue.put('reduce_dataset()')
            else:
                for i in range(2, self.num_datasets+1):
                    self.moveData(i, i-1)
                self.num_datasets -= 1
                self.instruction_queue.put('reduce_dataset()')
        elif index == 2:
            if self.num_datasets == 2:
                self.ui.label_filepath2.setText('')
                self.ui.label_color2.setStyleSheet("background-color: " + str(None))
                self.color[index-1] = DEFAULT_DATASET_COLOR
                self.instruction_queue.put('remove_dataset({})'.format(2))
                self.num_datasets -= 1
                self.instruction_queue.put('reduce_dataset()')
            else:
                for i in range(3, self.num_datasets+1):
                    self.moveData(i,i-1)
                self.num_datasets -= 1
                self.instruction_queue.put('reduce_dataset()')
        elif index == 3:
            if self.num_datasets == 3:
                self.ui.label_filepath3.setText('')
                self.ui.label_color3.setStyleSheet("background-color: " + str(None))
                self.color[index-1] = DEFAULT_DATASET_COLOR
                self.instruction_queue.put('remove_dataset({})'.format(3))
                self.num_datasets -= 1
                self.instruction_queue.put('reduce_dataset()')
            else:
                for i in range(4, self.num_datasets+1):
                    self.moveData(i,i-1)
                self.num_datasets -= 1
                self.instruction_queue.put('reduce_dataset()')
        elif index == 4:
            if self.num_datasets == 4:
                self.ui.label_filepath4.setText('')
                self.ui.label_color4.setStyleSheet("background-color: None")
                self.color[index-1] = DEFAULT_DATASET_COLOR
                self.instruction_queue.put('remove_dataset({})'.format(4))
                self.num_datasets -= 1
                self.instruction_queue.put('reduce_dataset()')
        print(self.valid_filenames)

    def remove1(self):
        """Button method for removing dataset 1"""
        self.remove_dataset(1)

    def remove2(self):
        """Button method for removing dataset 2"""
        self.remove_dataset(2)

    def remove3(self):
        """Button method for removing dataset 3"""
        self.remove_dataset(3)

    def remove4(self):
        """Button method for removing dataset 4"""
        self.remove_dataset(4)

    def moveData(self, origin, target):
        """Part of remove dataset flow.  Method to handle shifting datasets if removed dataset is not the last one
        """
        if origin == 2:
            self.ui.label_filepath1.setText(self.ui.label_filepath2.text())
            self.ui.label_filepath2.setText('')
            self.ui.label_color1.setStyleSheet("background-color: " + self.color[origin-1])
            self.ui.label_color2.setStyleSheet("background-color: None")
        elif origin == 3:
            self.ui.label_filepath2.setText(self.ui.label_filepath3.text())
            self.ui.label_filepath3.setText('')
            self.ui.label_color2.setStyleSheet("background-color: " + self.color[origin-1])
            self.ui.label_color3.setStyleSheet("background-color: None")
        elif origin == 4:
            self.ui.label_filepath3.setText(self.ui.label_filepath4.text())
            self.ui.label_filepath4.setText('')
            self.ui.label_color3.setStyleSheet("background-color: " + self.color[origin-1])
            self.ui.label_color4.setStyleSheet("background-color: None")
        self.color[target-1] = self.color[origin-1]
        self.color[origin-1] = DEFAULT_DATASET_COLOR
        self.instruction_queue.put('move_dataset({},{})'.format(origin, target))

    def select_color(self, index):
        """Promp user for color selection
        """
        self.color[index] = QtGui.QColorDialog.getColor().name()
        if index == 0:
            self.ui.label_color1.setStyleSheet("background-color: " + self.color[index])
        elif index == 1:
            self.ui.label_color2.setStyleSheet("background-color: " + self.color[index])
        elif index == 2:
            self.ui.label_color3.setStyleSheet("background-color: " + self.color[index])
        elif index == 3:
            self.ui.label_color4.setStyleSheet("background-color: " + self.color[index])

    def select_color1(self):
        self.select_color(0)
    def select_color2(self):
        self.select_color(1)
    def select_color3(self):
        self.select_color(2)
    def select_color4(self):
        self.select_color(3)

    def plot_all(self):
        """Tell worker thread to prepare data for plotting
        """
        self.instruction_queue.put('plot_all()')

    def import_dataset(self):
        """Send information to worker thread to load new dataset.  User to define colorspace
        """
        filepath = ''
        filepath = QtGui.QFileDialog.getOpenFileName(self, 'Select LAB file','*.csv')
        if filepath == '':
            self.ui.statusbar.showMessage('Please Select a file!')
        else:
            if self.num_datasets < MAX_DATASETS:
                self.num_datasets += 1
                filename = filepath.split("/")
                self.set_filepathText(self.num_datasets, filename[-1])
                self.valid_filenames.append(filename[-1])
                self.ui.statusbar.showMessage('Loaded Dataset {}!'.format(self.num_datasets))
                self.instruction_queue.put("add_dataset(r'{}',r'{}')".format(filepath, self.ui.comboBox_importColorspace.currentText()))
            else:
                self.ui.statusbar.showMessage('Error: Cannot exceed 4 datasets!')

    def separate_make_colorspaces(self):
        """Convert LAB for each dataset and output separate file for each dataset
        """
        filepath = QtGui.QFileDialog.getExistingDirectory(self, 'Save location')
        self.instruction_queue.put("separate_make_colorspaces(r'{}', {})".format(filepath, self.valid_filenames))

    def merge_make_colorspaces(self):
        """Convert LAB for all dataseta nd output a single file with all datasets combined
        """
        filepath = QtGui.QFileDialog.getExistingDirectory(self, 'Save location')
        self.instruction_queue.put("merge_make_colorspaces(r'{}')".format(filepath))

    def set_filepathText(self, index, filepath):
        if index == 1:
            self.ui.label_filepath1.setText(filepath)
        elif index == 2:
            self.ui.label_filepath2.setText(filepath)
        elif index == 3:
            self.ui.label_filepath3.setText(filepath)
        elif index == 4:
            self.ui.label_filepath4.setText(filepath)
        else:
            self.ui.statusbar.showMessage('Error: Could not set filepath for new dataset!')

    def exit(self):
        self.instruction_queue.put('shutdown_worker()')
        self.ui.statusbar.showMessage('Exiting Application...')

        for thread in self.threads.values():
            thread.quit()

        self.close()

    def error_import_colorspace(self, error_message):
        # need to add remove dataset routine for error cases, maybe change statusbar to popup message
        if error_message == 'RGB':
            self.showErrorDialog('Warning', 'Error: Cannot load dataset, missing some or all RGB columns')
            self.worker_status('Error: Cannot load dataset, missing some or all RGB columns')
            self.remove_dataset(self.num_datasets)
        elif error_message == 'XYZ':
            self.showErrorDialog('Warning', 'Error: Cannot load dataset, missing some or all XYZ columns')
            self.worker_status('Error: Cannot load dataset, missing some or all XYZ columns')
            self.remove_dataset(self.num_datasets)
        elif error_message == 'Lab':
            self.showErrorDialog('Warning', 'Error: Cannot load dataset, missing some or all LAB columns')
            self.worker_status('Error: Cannot load dataset, missing some or all LAB columns')
            self.remove_dataset(self.num_datasets)
        elif error_message == 'xy':
            self.showErrorDialog('Warning', 'Error: Cannot load dataset, missing some or all xy columns')
            self.worker_status('Error: Cannot load dataset, missing some or all xy columns')
            self.remove_dataset(self.num_datasets)

    def worker_status(self,string):
        self.ui.statusbar.showMessage(string)

    def showErrorDialog(self, type, text):
        msg = QtGui.QMessageBox()
        if type == 'Warning':
            msg.setIcon(QMessageBox.Warning)
        elif type == 'Information':
            msg.setIcon(QMessageBox.Information)
        elif type == 'Question':
            msg.setIcon(QMessageBox.Question)
        elif type == 'Critical':
            msg.setIcon(QMessageBox.Critical)

        msg.setText(text)
        msg.setWindowTitle("Import Error!")
        msg.setStandardButtons(QMessageBox.Ok)
        retval = msg.exec_()

def main():
    ui = PlotLABApp()
    app_icon = QtGui.QIcon()
    app_icon.addFile('resources/icon/256.png', QtCore.QSize(256,256))
    ui.setWindowIcon(app_icon)
    ui.show()


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    

    window = main()
    app.exec_()